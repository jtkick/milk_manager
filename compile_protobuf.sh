#!/usr/bin/bash

SRC_DIR=./milk_man_protocol/
DST_DIR=./milk_manager/

# Remove old proto file if it exists
rm $DST_DIR/messages_pb2.py 2> /dev/null

# Compile
protoc -I=$SRC_DIR --python_out=$DST_DIR $SRC_DIR/messages.proto
