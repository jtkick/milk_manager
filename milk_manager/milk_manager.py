#!/usr/bin/env python3

import dotenv
import logging
import sys

# Import Milk Manager modules
import discord_manager
import zmq_manager

def main():

    # Load environment variables
    dotenv.load_dotenv()

    # Setup logging
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)
    console_handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter("[ %(asctime)s ] [ %(name)s ] %(message)s")
    console_handler.setFormatter(formatter)
    root_logger.addHandler(console_handler)

    # Kindly shut the fuck up
    logging.getLogger("discord.client").setLevel(logging.WARNING)
    logging.getLogger("discord.gateway").setLevel(logging.WARNING)

    # Start dependent threadss
    discord_manager.start()
    zmq_manager.start()

    _ = input('')

    # Stop threads
    discord_manager.stop()
    zmq_manager.stop()


if __name__ == "__main__":

    main()
