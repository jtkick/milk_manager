#!/usr/bin/env python3

import asyncio
from concurrent.futures import ThreadPoolExecutor
import discord
import logging
import multiprocessing
import os
import threading
import time
import uuid

import messages_pb2
import milk_tank

# Create Discord bot
intents = discord.Intents.default()
intents.message_content = True
intents.guilds = True
client = discord.Client(intents=intents)

# Discord manager thread
thread = None

# Discord manager logger
logger = logging.getLogger('\x1b[38;2;114;137;218mDISCORD MANAGER\u001b[0m')

# Setup and start discord client without blocking main thread
# This function comes from https://stackoverflow.com/questions/55030714/c-python-asyncio-running-discord-py-in-a-thread
# IgorZ is a steely-eyed missile man
def start():
    loop = asyncio.get_event_loop()
    token = os.getenv('TOKEN')
    logger.info(f'Logging in with token: {token}')
    loop.create_task(client.start(token))
    thread = threading.Thread(target=loop.run_forever).start()


def stop():
    thread.stop()


# Decorator used to call Discord manager functions synchronously from manager module
def async_to_sync(func):

    def wrapper(*args, **kwargs):

        # Run function in client control loop
        client.loop.create_task(func(*args, **kwargs))

    return wrapper


# Client is initialized
@client.event
async def on_ready():

    logger.info('Discord bot ready')


# Respond to messages or commands
@client.event
async def on_message(message):

    logger.info(f'Message posted: {message.content}')

    # Ignore messages posted by bot itself
    if message.author == client.user:
        return

    # Make sure discord bot is ready to go
    await client.wait_until_ready()

    # React to messages containing 'milk'
    if 'milk' in message.content.lower():
        logger.info('Milk detected: adding reaction')
        emoji = discord.utils.get(client.emojis, name='bucket_of_milk')
        await message.add_reaction(emoji)

    # If message posted in status channel, and it wasn't us, delete it
    if message.channel.id == os.getenv('STATUS_CHANNEL_ID'):
        if message.author != client.user:
            await message.delete()
            return

    # React to commands if in command prompt channel
    if message.channel.id == os.getenv('COMMAND_PROMPT_CHANNEL_ID'):

        logger.info('Message posted to command channel')

        # Parse args
        args = message.content.split(' ')
        if len(args) < 2:
            await message.channel.send('Error: Expects a verb and noun.')
            return

        # Make it a bit easier
        verb = args[0]
        noun = args[1]
        parameters = args[2:]

        # Execute commands
        if verb == 'stop':
            pass

        if verb == 'start':
            pass

        if verb == 'rename':
            pass

        if verb == 'create':
            if noun == 'account':

                if len(parameters) != 2:
                    await message.channel.send('Error: Must provide a username and password.')
                    return

                # Put account into database for use
                try:
                    milk_tank.insert_account(*parameters)
                except Exception as e:
                    await message.channel.send(f'Error: {str(e)}')

            elif noun == 'server':
                # Add server
                pass

        if verb == 'enable':
            if noun == 'server':
                # Find server in database and enable
                pass
            if noun == 'account':
                # Find account and enable
                pass

        if verb == 'disable':
            if noun == 'server':
                # Find server in database and disable
                pass
            if noun == 'account':
                # Find account and disable
                pass


# Takes protobuf status message, and updates embed in status channel if one exists for the worker, and creates a new
# embed if one doesn't already exist
@async_to_sync
async def update_worker_status(worker_uuid: uuid.UUID, zmq_worker_message: messages_pb2.WorkerStatus):

    # Make sure discord bot is ready to go
    await client.wait_until_ready()

    # Get all messages in status channel
    print(os.getenv('STATUS_CHANNEL_ID'))
    status_channel = client.get_channel(int(os.getenv('STATUS_CHANNEL_ID')))
    messages = [m async for m in status_channel.history(limit=200)]

    # Find message with this worker's UUID in the embed footer
    discord_worker_message = None
    for message in messages:
        if len(message.embeds) == 1 and str(worker_uuid) in str(message.embeds[0].footer.text):
            discord_worker_message = message
            break
    # A message doesn't exist for this worker yet, so contruct one
    else:
        # Start with embed
        embed = discord.Embed(title='Worker',
                              description='\U0001F535 Status unknown')
        embed.set_footer(text='Worker UUID: ' + str(worker_uuid))
        discord_worker_message = await status_channel.send(embed=embed)

    # Save message contents
    message_embed = discord_worker_message.embeds[0]

    # Now we have the correct embed, update to reflect new info
    # Start with worker status
    if zmq_worker_message.running == False:
        description = '\U0001F7E1 Idle'
        color = 0xF3C25D
    else:
        description = '\U0001F7E2 Running'
        color = 0x71AC5C

    # Copy embed
    new_embed = discord.Embed(title=message_embed.title,
                              color=color)
    new_embed.set_footer(text=str(message_embed.footer.text))

    # Add worker status
    new_embed.add_field(name='Worker Status:\t',
                        value=description,
                        inline=True)
    new_embed.add_field(name='\u200b',
                        value='\u200b',
                        inline=True)
    # Add location of server
    new_embed.add_field(name='Server location:',
                        value='Unknown',
                        inline=True)

    # Get account info iff logged in
    if zmq_worker_message.logged_in == True:
        username = '```' + str(zmq_worker_message.username) + '```'
        world_number = '```' + str(zmq_worker_message.world_number) + '```'
    else:
        username = '```N/A```'
        world_number = '```N/A```'

    new_embed.add_field(name='Current username:', value=username, inline=True)

    new_embed.add_field(name='\u200b',
                        value='\u200b', inline=True)

    new_embed.add_field(name='World number:', value=world_number, inline=True)

    # Update message to have new embeds
    await discord_worker_message.edit(embed=new_embed)
