#!/usr/bin/env python3

create_user_account_table = """
    CREATE TABLE IF NOT EXISTS user_account (
        user_account_id INTEGER PRIMARY KEY,
        username VARCHAR(255),
        first_name VARCHAR(255),
        last_name VARCHAR(255),
        email VARCHAR(255),
        password_hash VARCHAR(4000),
        password_salt VARCHAR(4000),
        password_date DATETIME
    )
    """

create_server_table = """
    CREATE TABLE IF NOT EXISTS server (
        server_id INTEGER PRIMARY KEY,
        user_account_id INTEGER
        name VARCHAR(255),
        location VARCHAR(255),
        enabled BOOL
    )
    """

# server_id will be added manually, so will the server row
create_worker_table = """
    CREATE TABLE IF NOT EXISTS worker (
        worker_id INTEGER PRIMARY KEY,
        server_id INTEGER,
        uuid VARCHAR(255) NOT NULL,
        enabled BOOL
    )
    """

create_game_account_table = """
    CREATE TABLE IF NOT EXISTS game_account (
        game_account_id INTEGER PRIMARY KEY,
        username VARCHAR(255),
        password VARCHAR(255),
        enabled BOOL
    )
    """

create_status_table = """
    CREATE TABLE IF NOT EXISTS status (
        status_id INTEGER PRIMARY KEY,
        worker_id INTEGER NOT NULL,
        game_account_id INTEGER,
        date DATETIME NOT NULL,
        running BOOL NOT NULL,
        logged_in BOOL NOT NULL,
        logistics_state INTEGER NOT NULL,
        world_number INTEGER,
        in_game_location_x INTEGER,
        in_game_location_y INTEGER,
        in_game_location_z INTEGER,
        screenshot VARBINARY
    )
    """

create_exchange_table = """
    CREATE TABLE IF NOT EXISTS exchange (
        exchange_id INTEGER PRIMARY KEY,
        worker_id INTEGER NOT NULL,
        account_id INTEGER NOT NULL,
        date DATETIME
    )
    """

create_item_group_table = """
    CREATE TABLE IF NOT EXISTS item_group (
        item_group_id INTEGER PRIMARY KEY,
        exchange_id INTEGER NOT NULL,
        item_id INTEGER NOT NULL,
        quantity INTEGER NOT NULL,
        acquired BOOL NOT NULL
    )
    """

# create_item_table = """
#     CREATE TABLE IF NOT EXISTS item (
#         item_id INTEGER NOT NULL,
#         item_ingame_id INTEGER NOT NULL,
#         name VARCHAR(255)
#     )
#     """
