#!/usr/bin/env python3

class AccountAlreadyExistsError(Exception):

    def __init__(self):

        super().__init__('Username already exists in database.')

class NoMatchingAccountError(Exception):

    def __init__(self):

        super().__init__('No account idiot')

class DudeWhatTheHellError(Exception):

    def __init__(self):

        super().__init__('Dude, what the hell?')
