#!/usr/bin/env python3

# Nah
# import mysql.connector

import datetime
import logging
import sqlite3
import uuid

import messages_pb2
from milk_tank.schema import *
from milk_tank.exceptions import *

# Standardized SQL connection options
# HOST = 'localhost'
# USERNAME = <REDACTED>
# PASSWORD = <REDACTED>

# Make sure database is initialized
# connection = mysql.connector.connect(
#     host = HOST,
#     user = USERNAME,
#     password = '',
#     database='milk_tank'
# )

# Class for accessing database
class MilkTank:

    def __init__(self):
        pass


    def __enter__(self):

        # Establish connection
        connection = sqlite3.connect('test.db')
        cursor = connection.cursor()

        return cursor


    # Make sure account is logged out
    def __exit__(self, exception_type, exception_value, traceback):

        # TODO: LOGOUT OF ACCOUNT

        pass

database_path = 'test.db'

connection = sqlite3.connect('test.db')
cursor = connection.cursor()

# with connection as cursor:

# Initialize all tables
cursor.execute(create_user_account_table)
cursor.execute(create_server_table)
cursor.execute(create_worker_table)
cursor.execute(create_game_account_table)
cursor.execute(create_status_table)
cursor.execute(create_exchange_table)
cursor.execute(create_item_group_table)
connection.commit()


# Database logger
logging.getLogger('\u001b[34mMILK TANK\u001b[0m')


def insert_account(username: str, password: str):
    """Inserts given username and password pair into the milk-man database for use by workers.

    :param username: RuneScape account username
    :type username: str
    :param password: password used for the given username
    :type password: str
    """
    # Establish connection
    with sqlite3.connect(database_path) as connection:
        cursor = connection.cursor()

        # Make sure account username doesn't already exist in table
        command = """SELECT * FROM account WHERE username=?"""
        values = (username,)
        cursor.execute(command, values)
        accounts = cursor.fetchall()
        if len(accounts) != 0:
            raise AccountAlreadyExistsError()

        # Insert account into database
        command = """INSERT INTO account(username, password, enabled) VALUES (?, ?, ?)"""
        values = (username, password, True)
        cursor.execute(command, values)
        connection.commit()


# Turn on usage of given account name
def enable_account(username: str):
    """Enables account with the given username.

    :param username: RuneScape account username
    :type username: str
    """

    cursor.execute('UPDATE account SET enabled = TRUE WHERE username=' + username)
    connection.commit()


# Turn off usage of given account name
def disable_account(username: str):
    """Disables account with the given username.

    :param username: RuneScape account username
    :type username: str
    """

    cursor.execute('UPDATE account SET enabled = FALSE WHERE username=' + username)
    connection.commit()


# Enable/disable server



# Get single valid account that is the least used of valid accounts
# Returns empty credentials_reponse message if none available
def get_account_credentials(max_minutes_per_day: int = 3 * 60) -> messages_pb2.Wrapper:

    # Establish connection
    with sqlite3.connect(database_path) as connection:
        cursor = connection.cursor()

        # Get all known accounts that aren't disabled
        cursor.execute("""SELECT account_id FROM account WHERE enabled""")
        account_ids = cursor.fetchall()
        account_ids = [row[0] for row in account_ids]   # Flatten

        # For each account, get amount of time it's been in use for the last 24 hours
        account_candidates = []
        for account_id in account_ids:

            # Get all status messages with this ID received in the last 24 hours
            cursor.execute("""SELECT status_id FROM status WHERE \"date\" >= date('now', '-1 days')
                              AND \"date\" < date('now') AND logged_in""")
            statuses = cursor.fetchall()

            # Add this account to possibilities with number of statuses if it hasn't been used too much
            # Since each status message will be sent every minute, we'll just let every message mean one minute of logged
            # in time, so we can just direct compare
            if len(statuses) < max_minutes_per_day:
                account_candidates.append((account_id, len(statuses)))

        # Sort possible candidates by the number of statuses, keeping only account id
        account_candidates = [id for id, _ in sorted(account_candidates, key=lambda item: item[1])]

        # Declare return message
        wrapper = messages_pb2.Wrapper()

        # Get account credentials if there are any candidates
        if len(account_candidates) > 0:

            # Use the first account id since it's valid and the least used
            account_id = account_candidates[0]

            # Get credentials with this id
            cursor.execute("""SELECT username, password FROM account WHERE account_id == %s""" % account_id)
            credentials = cursor.fetchall()
            if len(credentials) != 1:
                raise DudeWhatTheHellError()

            # Construct protobuf message
            wrapper.credentials_response.username = credentials[0][0]
            wrapper.credentials_response.password = credentials[0][1]

        # Otherwise, return empty credentials message to be sent to worker
        else:

            # Set message to have credentials_response field
            wrapper.credentials_response.username = ''

        return wrapper


# Add worker status message into database for monitoring
def insert_worker_status(worker_uuid: uuid.UUID,
                         message: messages_pb2.WorkerStatus):

    # Establish connection
    with sqlite3.connect(database_path) as connection:
        cursor = connection.cursor()

        # Find worker entry with this UUID
        cursor.execute('SELECT worker_id FROM worker WHERE uuid = ?',
                       (str(worker_uuid), ))
        worker_rows = cursor.fetchall()

        # If for whatever reason, this worker doesn't exist in the table, just
        # add it, it's probably not a big deal
        if len(worker_rows) == 0:
            cursor.execute('INSERT INTO worker(uuid, enabled) VALUES (?, ?)',
                           (str(worker_uuid), True))
            cursor.execute('SELECT worker_id FROM worker WHERE uuid = ?',
                           (str(worker_uuid), ))
            worker_rows = cursor.fetchall()

        # Dude, what the hell?
        elif len(worker_rows) > 1:
            raise DudeWhatTheHellError()

        # Got worker_id now, proceed as normal
        worker_id = worker_rows[0][0]

        # Get the account row ID based on the username that was used, if any
        account_id = None
        if message.username != '':
            # Find account entry with this username
            cursor.execute('SELECT game_account_id FROM game_account WHERE'
                           'username = ?', (message.username, ))
            account_rows = cursor.fetchall()

            # If no account exists for this username, something very bad
            # happened elsewhere, the worker got an account that we don't have
            if len(account_rows) == 0:
                raise NoMatchingAccountError()

            # Dude, what the hell?
            elif len(account_rows) > 1:
                raise DudeWhatTheHellError()

            # Only one account, proceed as normal
            account_id = account_rows[0][0]

        # TODO: WORK ON INSERTING SCREENSHOT INTO TABLE

        # Compile values for insertion
        values = (worker_id,
                  account_id,
                  'CURRENT_TIMESTAMP',
                  message.running,
                  message.logged_in,
                  message.logistics_state,
                  message.world_number,
                  message.in_game_location_x,
                  message.in_game_location_y,
                  message.in_game_location_z,
                  None)

        # Command used to enter status entry
        command = """
            INSERT INTO status(worker_id, game_account_id, date, running,
            logged_in, logistics_state, world_number, in_game_location_x,
            in_game_location_y, in_game_location_z, screenshot)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""

        # Now insert everything into table
        cursor.execute(command, values)
        connection.commit()
