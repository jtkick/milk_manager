"""
This module manages the ZMQ socket that receives messages from workers and
responds accordingly.
"""

__all__ = ['start', 'stop']
__version__ = '0.1.0'
__author__ = 'Jared Kick'

import logging
import threading
import uuid
import zmq

import discord_manager
import messages_pb2
import milk_tank

# Module logger
_logger = logging.getLogger('  \u001b[31mZMQ MANAGER\u001b[0m  ')

# ZMQ manager thread
_stop_flag = threading.Event()
_thread = None
_zmq_context = None


def _run(stop_flag: threading.Event):
    """ZMQ thread function."""
    # Initialize ZMQ socket
    global _zmq_context
    _zmq_context = zmq.Context()
    zmq_client = _zmq_context.socket(zmq.ROUTER)
    zmq_client.bind("tcp://*:48879")

    # Create poller, so we don't block Discord functionality
    poller = zmq.Poller()
    poller.register(zmq_client, zmq.POLLIN)

    # Run continuously, destruction of socket will stop the thread
    while True:

        # Receive incoming message
        _logger.info('Waiting for new message...')
        try:
            path, worker_uuid, message = zmq_client.recv_multipart()
            _logger.debug('Message received')
        except zmq.error.ContextTerminated:
            _logger.info('Context terminated; stopping ZMQ receiver thread...')
            break

        # Re-construct UUID from bytes
        worker_uuid = uuid.UUID(bytes=worker_uuid)

        # Parse message
        wrapper = messages_pb2.Wrapper()
        wrapper.ParseFromString(message)

        # Update given worker embed if we received a status update
        if wrapper.HasField('worker_status'):

            _logger.info('Received status message.')

            # Add status message to database
            milk_tank.insert_worker_status(worker_uuid, wrapper.worker_status)

            # Update Discord status message
            discord_manager.update_worker_status(worker_uuid, wrapper.worker_status)

        # Respond to credentials request
        elif wrapper.HasField('credentials_request'):

            print('Received credentials request.')

            # Get worker credentials from database
            account = milk_tank.get_account_credentials()

            # Serialize message
            credentials_message = account.SerializeToString()

            # For testing, don't look up account, just return test account credentials
            # credentials_message = messages_pb2.Wrapper()
            # credentials_message.credentials_response.username = 'jaredkick@gmail.com'
            # credentials_message.credentials_response.password = 'thenightmancometh'
            # credentials_message = credentials_message.SerializeToString()

            # Send account credentials back from whence it came
            zmq_client.send_multipart([path, worker_uuid.bytes, credentials_message])

        # Keep track of account transactions
        elif wrapper.HasField('item_exchange'):

            print('Received item exchange message.')

            # Insert transaction into database
            milk_tank.insert_item_exchange(wrapper.item_exchange)

        else:
            print('oh no, oh god')


def start():
    """Starts the ZMQ listener thread."""
    global _thread
    if _thread: return
    _thread = threading.Thread(target=_run, args=(_stop_flag,))
    _thread.start()


def stop():
    """Stop the ZMQ listener thread."""
    global _thread
    if not _thread: return
    _thread.stop()
    _zmq_context.term()
    _thread.join()
    _thread = None
